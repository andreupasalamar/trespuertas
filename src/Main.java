import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    static Random rand = new Random();

    static int TRIES = 10000;
    static int SERIES = 3;
    public static void main(String[] args) {

        for (int j = 0; j < SERIES; j++){
            AtomicInteger wins = new AtomicInteger(0);
            for (int i = 0; i < TRIES; i++) game(rand, wins);
            System.out.println("WIN RATIO " + (double) wins.get()/TRIES * 100.00 + "%");
        }

    }

    private static void game(Random rand, AtomicInteger wins) {

        List<Door> doors = Arrays.asList(
                new Door(new Object(Type.GOAT), 0),
                new Door(new Object(Type.GOAT), 1),
                new Door(new Object(Type.GOAT), 2)
        );

        int option = rand.nextInt(3);
        boolean swapped = false;
        int carOption = rand.nextInt(3);
        doors.get(carOption).getObject().setType(Type.CAR);

        boolean open = false;
        int openOption;
        while (!open) {
            openOption = rand.nextInt(3);
            if (openOption != carOption && openOption != option) {
                doors.get(openOption).setOpen(true);
                open = true;
            }
        }

        for (Door door : doors) {
            if (!swapped && !door.isOpen() && door.getNumber() != option) {
                option = door.getNumber();
                swapped = true;
            }
        }

        if (option == carOption) {
            wins.set(wins.get() + 1);
        }
    }
}
