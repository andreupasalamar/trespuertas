public class Door {

    private final int number;
    private final Object object;
    private boolean isOpen = false;

    public Door(Object object, int number) {
        this.object = object;
        this.number = number;
    }

    public Object getObject() {
        return object;
    }


    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public int getNumber() {
        return number;
    }

}
